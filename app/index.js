import 'purecss';
import './main.css';
import component from './component';

// let's start to write a hot module replacement

let demoComponent = component();

document.body.appendChild(demoComponent);

if(module.hot){
  // ok, we can make purejs supports with hot module reload
  // by writing customized javascript
  module.hot.accept('./component', () => {
    const nextComponent = require('./component').default();

    document.body.replaceChild(nextComponent, demoComponent);

    demoComponent = nextComponent;
  });
}
