const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const PurifyCSSPlugin = require('purifycss-webpack');


exports.purifyCSS = function(paths){
  return {
    plugins: [
      new PurifyCSSPlugin({paths: paths}),
    ],
  };
};

exports.devServer = function(options){
  return {
    devServer: {
      historyApiFallback: true,

      hotOnly: true,
      stats: 'errors-only',
      host: options.host,
      port: options.port,
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin({}),
    ],
  };
};

exports.extractCSS = function(paths){
  return {
    module: {
      rules: [
        {
          test: /\.css$/,
          include: paths,
          loader: ExtractTextPlugin.extract({
            fallbackLoader: 'style-loader',
            loader: 'css-loader',
          }),
        },
      ],
    },
    plugins: [
      new ExtractTextPlugin('[name].css'),
    ],
  };
};

exports.lintJavaScript = function({path, options}){
  return {
    module: {
      rules: [
        {
          test: /\.js$/,
          include: path,
          enforce: 'pre',

          loader: 'eslint-loader',
          options: options,
        },
      ],
    },
  };
};

exports.loadCSS = function(paths){
  return {
    module: {
      rules: [
        {
          test: /\.css$/,
          exclude: /app/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.css$/,
          include: paths,
          exclude: /node_modules/,
          use: ['style-loader',
                'css-loader?&modules',
                {
                  loader: 'postcss-loader',
                  options: {
                    ident: 'postcss',
                    plugins: function(){
                      return [
                        require('stylelint')({
                          rules: {
                          "color-hex-case": "lower"
                        }}),
                        require('postcss-reporter')({clearMessages: true}),
                        require('postcss-cssnext'), // cssnext contain the autoprefixer
                      ];
                    },
                  },
                },
               ],
        },
      ],
    },
  };
};

