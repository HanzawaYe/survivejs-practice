# survivejs note

ＷＤＳ（webpack dev server) is a development server running in-memory. It supports an advanced webpack feature known as Hot Module Replacement (HMR).

HMR seems only to support React. However, we can make it to support pure js by wirting interface?

OK, I find webpack-dev-server only use hard refresh tech when js, html changed.

In mac, the `netstat` command works a little differently.

```sh

netstat -na | grep 8080
# in linux, I often use netstat -tulpn

```

Currently, style-loader and react, redux are easy to interact with HMR (Hot Module Replacement)

`webpack-dev-server --hot` use this to achieve Hot Module Replacement from the CLI.

OMG, we can go to **example.com/webpack-dev-server/** to see the app status information.


### eslint


 - 0 The rule has been disabled
 - 1 the rule will emit a warning
 - 2 the rule will emit an error
 
we can also tell ESLint to ignore files by .gitignore through `--ignore-path .gitignore` 

### the way to manage webpack configuration

1. maintain the configuration in multiple files.

share configuration through module imports and point webpack to use certain config file by the --config parameter.

2. maintain configuration within a single file and branch there by relying on the `--env` parameter.


#### there is no need to install `webpack-validator`

webpack2 validates the configuration by default.


### loaders

loaders are transformations that are applied to source files and return the new source. Loaders can be chained together, like using a pipe in Unix.

ex.

loaders: ['style-loader', 'css-loader'] is the same as

styleLoader(cssLoader(input))


### postcss hot reload

#### what postcss-import do


`ExtractTextPlugin` generate a separate CSS bundles and it won't work with Hot Module Replacement (HMR)

### progress
http://survivejs.com/webpack/handling-styles/linting/

# unresolve problem

 - hot module reload can not work properly with css 
 
